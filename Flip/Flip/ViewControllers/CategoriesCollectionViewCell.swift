//
//  CategoriesCollectionViewCell.swift
//  Flip
//
//  Created by Andrew McTague on 3/5/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    
    // Outlets for the items on the cells.
    @IBOutlet weak var featuredImageView: UIImageView!
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var backgroundColorView: UIView!

    // Tell the app how the cells should be set up and uopdate UI
    var category: Categories? {
        didSet{
            self.updateUI()
        }
    }
    
    private func updateUI()
    {
        
        if let category = category {
            featuredImageView.image = category.featuredImage
            categoryTitleLabel.text = category.title
            backgroundColorView.backgroundColor = category.color
        } else {
            featuredImageView.image = nil
            categoryTitleLabel.text = nil
            backgroundColorView.backgroundColor = nil
            
        }
        
    }
    // set the layout radious of the cells
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 3.0
        layer.shadowRadius = 10
        layer.shadowOpacity = 0.4
        layer.shadowOffset = CGSize(width: 5, height: 10)
        clipsToBounds = false
        
    }
    
}
