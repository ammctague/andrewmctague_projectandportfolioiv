//
//  TimerViewController.swift
//  Flip
//
//  Created by Andrew McTague on 3/12/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    
    var timer = Timer()
    var counter = 30.0
    var isRunning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timerLabel.text = "\(counter)"
        startButton.isEnabled = true
        pauseButton.isEnabled = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetTimer(_ sender: Any) {
        
        timer.invalidate()
        isRunning = false
        counter = 30.0
        timerLabel.text = "\(counter)"
        pauseButton.isEnabled = false
        startButton.isEnabled = true
        
    }

    @IBAction func startTimer(_ sender: Any) {
        if !self.isRunning {
            self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(TimerViewController.updateTimer), userInfo: nil, repeats: true)
            
        }
    
        startButton.isEnabled = false
        pauseButton.isEnabled = true
        isRunning = true
        
    }
    @IBAction func pauseTimer(_ sender: Any) {
        
        startButton.isEnabled = true
        pauseButton.isEnabled = false
        
        timer.invalidate()
        isRunning = false
        
    }
    @objc func updateTimer() {
        
        self.counter -= 1.0
        self.timerLabel.text = String(format: "%.1f", self.counter)
        
        if self.counter == 0 {
            self.timer.invalidate()
            
            let alert = UIAlertController(title: "Time is up", message: "Your app use timer has ended.", preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            self.startButton.isEnabled = false
            self.pauseButton.isEnabled = false
            self.resetButton.isEnabled = false
        }
    }
}
