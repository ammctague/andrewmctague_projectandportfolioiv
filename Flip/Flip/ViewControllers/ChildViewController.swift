//
//  ChildViewController.swift
//  Flip
//
//  Created by Andrew McTague on 3/14/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit

class ChildViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
