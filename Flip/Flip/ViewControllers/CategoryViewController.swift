//
//  CategoryViewController.swift
//  Flip
//
//  Created by Andrew McTague on 3/5/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {
    
    // create outlets for UI items.
    @IBOutlet weak var timerButton: UIBarButtonItem!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    
    // create variables to handle menus state.
    var sideMenuOpen = false
    
    // Create the categories to display.
    var categories = Categories.fetchCategories()
    let cellScaling: CGFloat = 0.6
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set the screen size of the collection view cells.
        let screenSize = UIScreen.main.bounds.size
        let cellWidth = floor(screenSize.width * cellScaling)
        let cellHeight = floor(screenSize.height * cellScaling)
        
        let insetX = (view.bounds.width - cellWidth) / 2.0
        let insetY = (view.bounds.height - cellHeight) / 2.0
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        collectionView?.contentInset = UIEdgeInsets(top: insetY, left: insetX, bottom: insetY, right: insetX)
        
        collectionView?.dataSource = self
        collectionView?.delegate = self
        
        // create the appearance of what the nav controller should present as.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        menuView.layer.shadowOpacity = 1
        menuView.layer.shadowRadius = 6
        
    }
    // Menu button tapped present menu if not open, and if open close.
    @IBAction func menuButton(_ sender: Any) {
        
        if (sideMenuOpen) {
            sideMenuConstraint.constant = -397.5
            
            // animate transition
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.layoutIfNeeded()
            })
            
        } else {
            sideMenuConstraint.constant = 0
            
            // animate transition
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.layoutIfNeeded()
            })
            
        }
        sideMenuOpen = !sideMenuOpen
    }
    // Menu to be closed if on home screen and home button is selected.
    @IBAction func homeButton(_ sender: Any) {
        
        if (sideMenuOpen) {
            sideMenuConstraint.constant = -397.5
            
            // animate transition
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.layoutIfNeeded()
            })
        }
        sideMenuOpen = !sideMenuOpen
    }
    
    
}
// Set the identity of the cells to appear in the view.
extension CategoryViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    // Set up the cells.
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as!CategoriesCollectionViewCell
        
        cell.category = categories[indexPath.item]
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // based on cell selected present the appropriate view controller.
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if indexPath.row == 0 {
            let dest = storyBoard.instantiateViewController(withIdentifier: "ABC")
            navigationController?.pushViewController(dest, animated: true)
        } else if indexPath.row == 1 {
            let dest = storyBoard.instantiateViewController(withIdentifier: "Num")
            navigationController?.pushViewController(dest, animated: true)
        } else if indexPath.row == 2 {
            let dest = storyBoard.instantiateViewController(withIdentifier: "Test")
            navigationController?.pushViewController(dest, animated: true)
        }
        
        
    }
}
// Set up the the state of the scroll view and the space between cells.
extension CategoryViewController: UIScrollViewDelegate, UICollectionViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let layout = self.collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        var offest = targetContentOffset.pointee
        let index = (offest.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        let roundIndex = round(index)
        
        offest = CGPoint(x: roundIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offest
        
    }
    
    
    
    
}
