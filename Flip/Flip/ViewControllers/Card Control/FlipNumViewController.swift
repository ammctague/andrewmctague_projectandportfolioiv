//
//  FlipNumViewController.swift
//  Flip
//
//  Created by Andrew McTague on 3/6/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit

class FlipNumViewController: UIViewController {
    
    // Timer variables and outlets
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var fifteenMinTimerButton: UIButton!
    @IBOutlet weak var thirtyMinTimerButton: UIButton!
    var timer = Timer()
    var counterFifteen = 900.00
    var counterThirty = 1800.00
    var isRunning = false
    
    @IBOutlet weak var cardView: UIView!
    static let initialState:State = .character
    
    // Main view outlets and variables.
    private var numbers: Numbers
    private var number: Number
    private var state: State
    private var previousNumber: Number
    
    required init?(coder aDecoder: NSCoder) {
        self.state = FlipNumViewController.initialState
        
        self.numbers = Numbers()
        self.number = numbers.nextNumber()
        self.previousNumber = numbers.prevNumber()
        super.init(coder: aDecoder)
        
    }
    
    @IBOutlet weak var SpeakButton: UIButton!
    @IBOutlet weak var ModeButton: UIButton!
    @IBOutlet weak var NumberDisplay: UILabel!
    @IBOutlet weak var PronunciationDisplay: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        setupLabels()
        setupButton()
        nextNumber()
        
        cardView.layer.cornerRadius = 10
        cardView.layer.shadowOpacity = 1
        
        resetButton.isHidden = true
        timerLabel.text = "00:00"
        
    }
    // Funtion to swap from telephony to number
    @IBAction func ScreenTapped(_ sender: UITapGestureRecognizer) {
        toggleFlashcard()
    }
    // move to next option
    @IBAction func NextButtonTapped(_ sender: UIButton) {
        nextNumber()
    }
    // move to previous option
    @IBAction func PreviousButtonTapped(_ sender: UIButton){
        prevNumber()
    }
    // Tap to hear pronuciation of letter or number.
    @IBAction func AudioButtonTapped(_ sender: UIButton) {
        if isShowingCharacter() {
            number.sayCharacter()
        } else {
            number.sayPronunciation()
        }
    }
    // Switch modes from ordered to random
    @IBAction func ModeButtonTapped(_ sender: UIButton) {
        numbers.switchMode()
        setupButton()
    }
    // Toggle state
    func toggleState() {
        if self.state == .character {
            self.state = .telephony
        } else {
            self.state = .character
        }
    }
    
    func isShowingCharacter() -> Bool {
        return self.state == .character
    }
    
    func isShowingTelephony() -> Bool {
        return self.state == .telephony
    }
    
    func mainLableText() -> String {
        if isShowingCharacter() {
            return number.character
        } else {
            return number.telphony
        }
    }
    
    func nextNumber() {
        setupInitialState()
        self.number = self.numbers.nextNumber()
        setupLabels()
    }
    
    func prevNumber() {
        setupInitialState()
        self.number = self.numbers.prevNumber()
        setupLabels()
    }
    
    func toggleFlashcard() {
        toggleState()
        setupLabels()
    }
    
    func setupInitialState() {
        self.state = FlipNumViewController.initialState
    }
    
    func setupLabels() {
        self.NumberDisplay.text = mainLableText()
        self.PronunciationDisplay.isHidden = isShowingCharacter()
        self.PronunciationDisplay.text = number.pronunciation
    }
    func setupButton() {
        self.ModeButton.setTitle(self.numbers.humanReadableMode(), for: .normal)
    }
    
    // MARK: - TImer Functions
    @IBAction func thirtyMinTimer(_ sender: Any) {
        // Start timer if not running
        if !self.isRunning {
            self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(FlipAbcController.updateTimerTwo), userInfo: nil, repeats: true)
            
        }
        isRunning = true
        fifteenMinTimerButton.isEnabled = false
        thirtyMinTimerButton.isEnabled = false
        pauseButton.isHidden = false
    }
    @IBAction func fifteenMinTimer(_ sender: Any) {
        // Start timer if not running
        if !self.isRunning {
            self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(FlipAbcController.updateTimer), userInfo: nil, repeats: true)
            
        }
        isRunning = true
        fifteenMinTimerButton.isEnabled = false
        thirtyMinTimerButton.isEnabled = false
        pauseButton.isHidden = false
        
    }
    // Allow the user to pause timer
    @IBAction func pauseTimer(_ sender: Any) {
        
        fifteenMinTimerButton.isEnabled = true
        thirtyMinTimerButton.isEnabled = true
        timer.invalidate()
        isRunning = false
        
    }
    // Reset at the end of time
    @IBAction func resetTimer(_ sender: Any) {
        
        timer.invalidate()
        isRunning = false
        counterFifteen = 900.00
        counterThirty = 1800.00
        timerLabel.text = "00:00"
        pauseButton.isEnabled = true
        pauseButton.isHidden = false
        resetButton.isHidden = true
        fifteenMinTimerButton.isEnabled = true
        
    }
    // Set up how the timer is displayed and count down.
    func formatingThings(_ seconds: TimeInterval) -> String {
        if seconds.isNaN {
            return "00:00"
        }
        let Min = Int(seconds / 60)
        let Sec = Int(seconds.truncatingRemainder(dividingBy: 60))
        return String(format: "%02d:%02d", Min, Sec)
        
    }
    // How the timer is viewed by the user and updates on each second gone by.
    @objc func updateTimerTwo() {
        self.counterThirty -= 0.1
        let convert = formatingThings(TimeInterval(counterThirty))
        self.timerLabel.text = String(convert)
        
        if self.counterThirty == 0 {
            self.timer.invalidate()
            
            let alert = UIAlertController(title: "Time is up", message: "Your app use timer has ended.", preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            pauseButton.isHidden = true
            resetButton.isHidden = false
            
        }
    }
    // How the timer is viewed by the user and updates on each second gone by.
    @objc func updateTimer() {
        
        self.counterFifteen -= 0.1
        let convert = formatingThings(TimeInterval(counterFifteen))
        self.timerLabel.text = String(convert)
        
        if self.counterFifteen == 0 {
            self.timer.invalidate()
            
            let alert = UIAlertController(title: "Time is up", message: "Your app use timer has ended.", preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            pauseButton.isHidden = true
            resetButton.isHidden = false
            
        }
    }
}



