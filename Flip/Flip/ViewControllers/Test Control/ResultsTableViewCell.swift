//
//  ResultsTableViewCell.swift
//  Flip
//
//  Created by Andrew McTague on 3/14/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit

class ResultsTableViewCell: UITableViewCell {

    // How the results present.
    @IBOutlet weak var resultQuestionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
