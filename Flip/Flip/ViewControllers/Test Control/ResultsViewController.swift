//
//  ResultsViewController.swift
//  Flip
//
//  Created by Andrew McTague on 3/14/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // Variables and outlets
    var missedAnswers = [String]()
    var wrong = Int()
    var right = Int()
    
    @IBOutlet weak var resultLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getResultsDisplay()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Set up the tableview to show missed questions for to practice.
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return missedAnswers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! ResultsTableViewCell
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.cyan
        } else {
            cell.backgroundColor = UIColor.darkGray
        }
        cell.resultQuestionLabel.text = "Question Missed:\n \(missedAnswers[indexPath.row])"
        
        return cell
    }
    func getResultsDisplay() {
        if right - wrong <= 0 {
             resultLabel.text = "0/10"
        } else {
        resultLabel.text = "\(right - wrong)" + "/" + "\(right)"
            
        }
    }

}
