//
//  TestViewController.swift
//  Flip
//
//  Created by Andrew McTague on 3/14/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit

struct Question {
    var Question: String!
    var Answers: [String]!
    var Answer: Int!
}

class TestViewController: UIViewController {
    
    // Outlets for the test view
    @IBOutlet weak var resultsButton: UIButton!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet var Buttons: [UIButton]!
    @IBOutlet weak var buttonOne: UIButton!
    @IBOutlet weak var buttonTwo: UIButton!
    @IBOutlet weak var buttonThree: UIButton!
    @IBOutlet weak var buttonFour: UIButton!
    
    // Variables to control test
    var Questions = [Question]()
    var qNumber = Int()
    var aNumber = Int()
    var rightAnswer = 0
    var wrongAnswer = 0
    var answersMissed = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultsButton.isHidden = true
        // Cretaed questions
        Questions = [
            Question(Question: "APPLE, starts with what letter?.", Answers: ["A","B","C","D"], Answer: 0),
            Question(Question: "1..2..3.._ \nWhat number comes next?.", Answers: ["1","5","4","6"], Answer: 2),
            Question(Question: "A B C _ \nWhat is the next letter?", Answers: ["A","B","C","D"], Answer: 3),
            Question(Question: "Banana starts with what letter?.", Answers: ["A","B","C","D"], Answer: 1),
            Question(Question: "D \nWhat letter is this?.", Answers: ["A","B","C","D"], Answer: 3),
            Question(Question: "Zebra starts with what letter.", Answers: ["A","X","Z","G"], Answer: 2),
            Question(Question: "G \nWhat letter matches this?.", Answers: ["D","P","G","Q"], Answer: 2),
            Question(Question: "L, M, _, O. \nWhat is the missing letter?", Answers: ["P","N","D","H"], Answer: 1),
            Question(Question: "7, 8, 9 _ \nWhat is the next number?.", Answers: ["10","11","12","13"], Answer: 0),
            Question(Question: "T, U, V, _ \nWhat is the next letter?.", Answers: ["F","W","X","J"], Answer: 1)]
        
        pickQuestion()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Send the stored results to another view
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "results" {
            let svc = segue.destination as! ResultsViewController
            svc.missedAnswers = answersMissed
            svc.wrong = wrongAnswer
            svc.right = rightAnswer
        }
    }
    // Set the questions to sequentially move forward
    func pickQuestion() {
        
        if Questions.count > 0 {
            qNumber = 0
            questionLabel.text = Questions[qNumber].Question
            aNumber = Questions[qNumber].Answer
            for i in 0..<Buttons.count {
                Buttons[i].setTitle(Questions[qNumber].Answers[i], for: .normal)
            }
            Questions.remove(at: qNumber)
        } else {
            
            resultsButton.isHidden = false
            buttonOne.isEnabled = false
            buttonTwo.isEnabled = false
            buttonThree.isEnabled = false
            buttonFour.isEnabled = false
            
        }
    }
    // Buttons that control if the answer is correct or not. Store the results.
    @IBAction func buttonOne(_ sender: Any) {
        if aNumber == 0 {
            rightAnswer += 1
            pickQuestion()
        } else {
            wrongAnswer += 1
            answersMissed.append(questionLabel.text!)
            print("Wrong")
            pickQuestion()
        }
    }
    @IBAction func buttonTwo(_ sender: Any) {
        if aNumber == 1 {
            rightAnswer += 1
            pickQuestion()
        } else {
            wrongAnswer += 1
            answersMissed.append(questionLabel.text!)
            print("Wrong")
            pickQuestion()
        }
    }
    @IBAction func buttonThree(_ sender: Any) {
        if aNumber == 2 {
            rightAnswer += 1
            pickQuestion()
        } else {
            wrongAnswer += 1
            answersMissed.append(questionLabel.text!)
            print("Wrong")
            pickQuestion()
        }
    }
    @IBAction func buttonFour(_ sender: Any) {
        if aNumber == 3 {
            rightAnswer += 1
            pickQuestion()
        } else {
            wrongAnswer += 1
            answersMissed.append(questionLabel.text!)
            print("Wrong")
            pickQuestion()
        }
    }
}

