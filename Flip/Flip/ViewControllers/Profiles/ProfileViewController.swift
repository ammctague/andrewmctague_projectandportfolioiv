//
//  ProfileViewController.swift
//  Flip
//
//  Created by Andrew McTague on 3/5/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit
import CoreData

class ProfileViewController: UIViewController, UITextFieldDelegate {
    
    // Outlets for the main profile controller
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var menuConstraint: NSLayoutConstraint!
    // control the menu
    var sideMenuOpen = false
    // hold the saved accounts
    var accounts = [Profile]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // Set the images and basic views of the profile data and adjust the nav bar appearance.
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        profileImageView.layer.masksToBounds = true
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        nameTextField.delegate = self
        nameTextField.tag = 0
        emailTextfield.delegate = self
        emailTextfield.tag = 1
        passwordTextField.delegate = self
        passwordTextField.tag = 2
        
    }
    // Control the menu options
    @IBAction func menuButton(_ sender: Any) {
        
        if (sideMenuOpen) {
            menuConstraint.constant = -397.5
            
            // animate transition
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.layoutIfNeeded()
            })
            
        } else {
            menuConstraint.constant = 0
            
            // animate transition
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.layoutIfNeeded()
            })
            
        }
        sideMenuOpen = !sideMenuOpen
    }
    // Save data that is entered to the app
    @IBAction func saveAccountButton(_ sender: Any) {
        
        if nameTextField.text != "" && emailTextfield.text != "" && passwordTextField.text != ""
        {
            let name = nameTextField.text
            let email = emailTextfield.text
            let password = passwordTextField.text
            
            let profile = Profile(context: PersistantService.context)
            
            profile.name = name
            profile.email = email
            profile.password = password
            
            PersistantService.saveContext()
            accounts.append(profile)
            
        }
        else
        {
            //TODO: - Update with alert...
            let alert = UIAlertController(title: "Empty Fields", message: "Please enter all fields", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            
        }
        
    }
    // Close the menu if profile is selected.
    @IBAction func profileButton(_ sender: Any) {
        if (sideMenuOpen) {
            menuConstraint.constant = -397.5
            
            // animate transition
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.layoutIfNeeded()
            })
            
        }
         sideMenuOpen = !sideMenuOpen
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if let nextField = nameTextField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
}
