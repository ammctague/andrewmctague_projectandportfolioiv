//
//  ChildTableViewCell.swift
//  Flip
//
//  Created by Andrew McTague on 3/19/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit

class ChildTableViewCell: UITableViewCell {

    //Outlets for tableview cell of child.
    @IBOutlet weak var childAgeLabel: UILabel!
    @IBOutlet weak var childNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
