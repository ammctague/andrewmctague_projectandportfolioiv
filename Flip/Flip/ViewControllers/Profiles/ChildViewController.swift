//
//  ChildViewController.swift
//  Flip
//
//  Created by Andrew McTague on 3/14/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit
import CoreData

class ChildViewController: UIViewController, UITextFieldDelegate {

    // Outlets to enter child data
    @IBOutlet weak var childNameTextField: UITextField!
    @IBOutlet weak var childAgeTextField: UITextField!
    
    // Save the child accounts
    var childAccounts = [Child]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        childNameTextField.delegate = self
        childNameTextField.tag = 0
        childAgeTextField.delegate = self
        childAgeTextField.tag = 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Action to store the entered data to the app
    @IBAction func saveChildAccount(_ sender: Any) {
        
        if childNameTextField.text != "" && childAgeTextField.text != ""
        {
            
            let childProfile = Child(context: PersistantService.context)
            
            let name = childNameTextField.text
            let age = childAgeTextField.text
            
            childProfile.name = name
            childProfile.age = age
            
            PersistantService.saveContext()
            childAccounts.append(childProfile)
            
        }
        else
        {
            //Alert the user if no data entered.
            let alert = UIAlertController(title: "Empty Fields", message: "Please enter all fields", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if let nextField = childNameTextField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
}
