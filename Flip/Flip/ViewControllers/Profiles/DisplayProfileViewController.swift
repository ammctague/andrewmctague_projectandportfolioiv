//
//  DisplayProfileViewController.swift
//  Flip
//
//  Created by Andrew McTague on 3/5/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit
import CoreData

class DisplayProfileViewController: UIViewController {
    
    // Outlets for the profile
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTextLabel: UILabel!
    @IBOutlet weak var emailTextLabel: UILabel!
    @IBOutlet weak var passwordTextLabel: UILabel!
    
    // Hold the created profiles
    var profile = [Profile]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the image view to be round and set the text to be blank if none set.
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        profileImageView.layer.masksToBounds = true
        nameTextLabel.text = ""
        emailTextLabel.text = ""
        passwordTextLabel.text = ""
        // call core data
        fetchData()
    }
    // Fetch the data stored in the app to present to the profile.
    func fetchData() {
        
        let fetchRequest: NSFetchRequest<Profile> = Profile.fetchRequest()
        do {
            let profiles = try PersistantService.context.fetch(fetchRequest)
            self.profile = profiles
            if profile.count >= 1 {
                for profile in profiles {
                    
                    nameTextLabel.text = profile.name
                    emailTextLabel.text = profile.email
                    passwordTextLabel.text = profile.password
                }
            }
            
        } catch {
            print(error)
        }
    }
    // Edit profile, or add new.
    @IBAction func editProfile(_ sender: Any) {
        let editProfileAlert = UIAlertController(title: "Edit Profile", message: "Enter some text below",preferredStyle: .alert)
        
        editProfileAlert.addTextField(configurationHandler: {(textField: UITextField!) in textField.placeholder = "Enter Name"})
        
        editProfileAlert.addTextField(configurationHandler: {(textField: UITextField!) in textField.placeholder = "Enter email"})
        
        editProfileAlert.addTextField(configurationHandler: {(textField: UITextField!) in textField.placeholder = "Enter password"})
        
        // Create the action to edit/update the profile and save it to the core data
        let action = UIAlertAction(title: "Submit",
                                   style: UIAlertActionStyle.default,
                                   handler: {[weak self]
                                    (paramAction:UIAlertAction!) in
                                    if let textFields = editProfileAlert.textFields{
                                        let theTextFields = textFields as [UITextField]
                                        let nameText = theTextFields[0].text
                                        let emailText = theTextFields[1].text
                                        let passText = theTextFields[2].text
                                        self?.nameTextLabel.text = nameText
                                        self?.emailTextLabel.text = emailText
                                        self?.passwordTextLabel.text = passText
                                        
                                    }
                                    let profile = Profile(context: PersistantService.context)
                                    
                                    profile.name = self?.nameTextLabel.text
                                    profile.email = self?.emailTextLabel.text
                                    profile.password = self?.passwordTextLabel.text
                                    
                                    PersistantService.saveContext()
                                    //accounts.append(profile)
        })
        // cancel the update.
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) { (action:UIAlertAction!) in print("Cancel button tapped") }
        
        editProfileAlert.addAction(cancelAction)
        editProfileAlert.addAction(action)
        self.present(editProfileAlert, animated: true, completion: nil)
    }
}
