//
//  ChildAccountTableViewController.swift
//  Flip
//
//  Created by Andrew McTague on 3/19/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit
import  CoreData

class ChildAccountTableViewController: UITableViewController {
    
    // Outlets and variables for the child table view.
    @IBOutlet weak var editNameTextField: UITextField!
    var childProfile = [Child]()
    var childAge = String()
    var childName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        fetchData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Gather the stroed child accounts
    func fetchData() {
        
        let fetchRequest: NSFetchRequest<Child> = Child.fetchRequest()
        do {
            let childProfiles = try PersistantService.context.fetch(fetchRequest)
            self.childProfile = childProfiles
            
        } catch {
            print(error)
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return childProfile.count
    }

    // Set the cells of the stored child accounts to stored data
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "childCell", for: indexPath) as! ChildTableViewCell

        // Configure the cell...
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.lightGray
        } else {
            cell.backgroundColor = UIColor.gray
        }
        
        cell.childNameLabel.text = childProfile[indexPath.row].name
        cell.childAgeLabel.text = childProfile[indexPath.row].age

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

}
