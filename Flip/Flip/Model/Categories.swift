//
//  Categories.swift
//  Flip
//
//  Created by Andrew McTague on 3/5/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit

class Categories
{
    // MARK: - Public API
    var title = ""
    var featuredImage: UIImage
    var color: UIColor
    
    init(title: String, featuredImage: UIImage, color: UIColor)
    {
        self.title = title
        self.featuredImage = featuredImage
        self.color = color
    }
    
    //MARK: - Private
    // Set up the current help categories
    static func fetchCategories() -> [Categories]
    {
        return [
            Categories(title: "ABC's", featuredImage: UIImage(named: "f1")!, color: UIColor(red: 63/255.0, green: 71/255.0, blue: 80/255.0, alpha: 0.8)),
            Categories(title: "Numbers", featuredImage: UIImage(named: "f2")!, color: UIColor(red: 240/255.0, green: 133/255.0, blue: 91/255.0, alpha: 0.8)),
             Categories(title: "Test", featuredImage: UIImage(named: "f3")!, color: UIColor(red: 200/255.0, green: 74/255.0, blue: 102/255.0, alpha: 0.8))
        ]
    }
}
