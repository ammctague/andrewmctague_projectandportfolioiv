//
//  Alphabet.swift
//  Flip
//
//  Created by Andrew McTague on 3/6/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import Foundation

enum Mode:Int {
    case ordered = 0
    case random
}

class Alphabet {
    
    // Variables and setup options for the class.
    static let characters = Array("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    static let modes:[Mode] = [.ordered,.random]
    
    var mode:Mode = .ordered
    var index = 0
    
    func nextLetter() -> Letter {
        return Letter(character: String(nextCharacter()))
    }
    
    func prevLetter() -> Letter {
        return Letter(character: String(prevCharacter()))
    }
    
    func switchMode() {
        
        switch self.mode {
        case .ordered:
            self.index = -1
            self.mode = .random
        case .random:
            self.mode = .ordered
        }
        
    }
    
    func humanReadableMode() -> String {
        return "\(self.mode)".capitalized
    }
    
    private
    
    func nextCharacter() -> Character {
        switch self.mode {
        case .ordered:
            return nextOrderedCharacter()
        case .random:
            return nextRandomCharacter()
        }
    }
    
    func prevCharacter() -> Character {
        switch self.mode {
            case .ordered:
            return prevOrderedCharacter()
            case .random:
            return nextRandomCharacter()
        }
    }
    
    func nextRandomCharacter() -> Character {
        let randomIndex: Int = Int(arc4random_uniform(UInt32(Alphabet.characters.count)))
        return Alphabet.characters[randomIndex]
    }
    
    func prevOrderedCharacter() -> Character {
        if self.index - 1 == Alphabet.characters.count {
            self.index = 0
        } else if self.index == 0 {
            self.index = 25
        } else {
            self.index -= 1
        }
        
        let prevCharacter = Alphabet.characters[self.index]
        return prevCharacter
    }
    
    func nextOrderedCharacter() -> Character {
        if self.index + 1 == Alphabet.characters.count {
            self.index = 0
        } else {
            self.index += 1
        }
        
        let nextCharacter = Alphabet.characters[self.index]
        return nextCharacter
    }
}
