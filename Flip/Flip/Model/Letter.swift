//
//  Letter.swift
//  Flip
//
//  Created by Andrew McTague on 3/6/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import UIKit
import AVFoundation

class Letter: NSObject, AVSpeechSynthesizerDelegate {
    // VARIABLES for AV functionality
    let character: String
    let telphony: String
    let pronunciation: String
    
    static var idx: Int = 0
    static let speechSynthesizer = AVSpeechSynthesizer()
    static let letters = [
        "A":["Ap-ple", "APPLE"],
        "B":["Ba-Na-Na","BANANA"],
        "C":["Ca-At","CAT"],
        "D":["Da-Og","DOG"],
        "E":["Eat","EAT"],
        "F":["Fa-Ox","FOX"],
        "G":["Ga-Alf","GOLF"],
        "H":["Hotel","HOH-TEL"],
        "I":["India","IN-DEE-AH"],
        "J":["Ja-Ar","JAR"],
        "K":["Kid","KID"],
        "L":["La-Amp","LAMP"],
        "M":["Moo-Se","MOOSE"],
        "N":["Nin-Ja","NINJA"],
        "O":["Oscar","OSS-CAH"],
        "P":["Papa","PAH-PAH"],
        "Q":["Qui-Et","Quiet"],
        "R":["Ro-Ome","ROME"],
        "S":["Sad","SAD"],
        "T":["Tan","TAN"],
        "U":["Unk-Le","Uncle"],
        "V":["Van","VAN"],
        "W":["Wha-Ale","WHALE"],
        "X":["X-ray","ECKS-RAY"],
        "Y":["Yes","YES"],
        "Z":["Ze-Bra","ZEBRA"]
    ]
    // setup the letters array for AV
    init(character:String) {
        self.character = character
        if let telephonyWithPronunciation = Letter.letters[character] {
            self.telphony = telephonyWithPronunciation[0]
            self.pronunciation = telephonyWithPronunciation[1]
        } else {
            fatalError("\"\(character)\" is not in range A-Z")
        }
    }
    // Set up speech for AV
    func sayCharacter() {
        
        if Letter.speechSynthesizer.isSpeaking{
            return
        }
        let utterance:AVSpeechUtterance = AVSpeechUtterance(string: self.character.lowercased())
        Letter.speechSynthesizer.speak(utterance)
    }
    func sayPronunciation() {
        if Letter.speechSynthesizer.isSpeaking {
            return
        }
        var string: String
        switch self.character {
        case "I","U","H","E","R","N","B","S":
            string = self.telphony
        default:
            string = self.pronunciation
        }
        let utterance:AVSpeechUtterance = AVSpeechUtterance(string: string)
        Letter.speechSynthesizer.speak(utterance)
        
    }
}
