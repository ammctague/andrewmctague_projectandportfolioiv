//
//  myTimer.swift
//  Flip
//
//  Created by Andrew McTague on 3/12/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import Foundation


let myTimerNotifyNameKey = "timerKey"
let myTimerNotifyUserInfoTimeKey = "timerUserInfoKey"

class myTimer {

    static let sharedInstance = myTimer()
    var timer: Timer
    var time = 0
    
    func startTimer() {
        
        time = 0
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: "timerFunc", userInfo: nil, repeats: true)
        
    }
    func stopTimer() {
        timer.invalidate()
        timer = nil
    }
    
    func timerFunc(timer: Timer) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: myTimerNotifyNameKey), object: nil, userInfo: {myTimerNotifyUserInfoTimeKey: time += 1})
    }
}
