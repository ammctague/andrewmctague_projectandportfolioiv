//
//  Number.swift
//  Flip
//
//  Created by Andrew McTague on 3/6/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import Foundation
import AVFoundation

class Number: NSObject, AVSpeechSynthesizerDelegate {
    
    let character: String
    let telphony: String
    let pronunciation: String
    
    static var idx: Int = 0
    static let speechSynthesizer = AVSpeechSynthesizer()
    static let numbers = [
        "1":["One", "WUN"],
        "2":["Two","TOO"],
        "3":["Three","TREE"],
        "4":["Four","FOW-ER"],
        "5":["Five","FIFE"],
        "6":["Six","SIX"],
        "7":["Seven","SEV-EN"],
        "8":["Eight","AIT"],
        "9":["Nine","NIN-ER"],
        "10":["Ten","WUN-ZERO"],
        "11":["Eleven","WUN-WUN"],
        "12":["Twelve","WUN-T00"],
        "13":["Thirteen","WUN-TREE"],
        "14":["Fourteen","WUN-FOW-ER"],
        "15":["Fifteen","ONE-FIFE"],
        "16":["Sixteen","WUN-SIX"],
        "17":["Seventeen","WUN-SEV-EN"],
        "18":["Eighteen","WUN-AIT"],
        "19":["Nineteen","WUN-NIN-ER"],
        "20":["Twenty","TOO-ZERO"],
        "21":["Twenty One","TOO-WUN"],
        "22":["Twenty Two","TOO-TOO"],
        "23":["Twenty Three","TOO-TREE"],
        "24":["Twenty Four","TOO-FOW-ER"],
        "25":["Twenty Five","TOO-FIFE"],
        "26":["Twenty Six","TOO-SIX"]
    ]
    
    init(character:String) {
        self.character = character
        if let telephonyWithPronunciation = Number.numbers[character] {
            self.telphony = telephonyWithPronunciation[0]
            self.pronunciation = telephonyWithPronunciation[1]
        } else {
            fatalError("\"\(character)\" is not in range 1-26")
        }
    }
    func sayCharacter() {
        
        if Number.speechSynthesizer.isSpeaking{
            return
        }
        let utterance:AVSpeechUtterance = AVSpeechUtterance(string: self.character.lowercased())
        Number.speechSynthesizer.speak(utterance)
    }
    func sayPronunciation() {
        if Number.speechSynthesizer.isSpeaking {
            return
        }
        var string: String
        switch self.character {
        case "I","U","H","E","R","N","B","S":
            string = self.telphony
        default:
            string = self.pronunciation
        }
        let utterance:AVSpeechUtterance = AVSpeechUtterance(string: string)
        Number.speechSynthesizer.speak(utterance)
        
    }
}
