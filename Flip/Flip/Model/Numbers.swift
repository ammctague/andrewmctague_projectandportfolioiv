//
//  Numbers.swift
//  Flip
//
//  Created by Andrew McTague on 3/6/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//

import Foundation

enum NumMode:Int {
    case ordered = 0
    case random
}

class Numbers {
    
    static let characters = Array(1...26)
    
    static let modes:[NumMode] = [.ordered,.random]
    
    var mode:NumMode = .ordered
    var index = 0
    
    func nextNumber() -> Number {
        return Number(character: String(nextCharacter()))
    }
    
    func prevNumber() -> Number {
        return Number(character: String(prevCharacter()))
    }
    
    func switchMode() {
        
        switch self.mode {
        case .ordered:
            self.mode = .random
        case .random:
            self.mode = .ordered
        }
        
    }
    
    func humanReadableMode() -> String {
        return "\(self.mode)".capitalized
    }
    
    private
    
    func nextCharacter() -> Int {
        switch self.mode {
        case .ordered:
            return nextOrderedCharacter()
        case .random:
            return nextRandomCharacter()
        }
    }
    
    func prevCharacter() -> Int {
        switch self.mode {
        case .ordered:
            return prevOrderedCharacter()
        case .random:
            return nextRandomCharacter()
        }
    }
    
    func nextRandomCharacter() -> Int {
        let randomIndex: Int = Int(arc4random_uniform(UInt32(Numbers.characters.count)))
        return Numbers.characters[randomIndex]
    }
    
    func prevOrderedCharacter() -> Int {
        if self.index - 1 == Numbers.characters.count {
            self.index = 0
        } else if self.index == 0 {
            self.index = 25
        } else {
            self.index -= 1
        }
        
        let prevCharacter = Numbers.characters[self.index]
        return prevCharacter
    }
    
    func nextOrderedCharacter() -> Int {
        if self.index + 1 == Numbers.characters.count {
            self.index = 0
        } else {
            self.index += 1
        }
        
        let nextCharacter = Numbers.characters[self.index]
        return nextCharacter
    }
}

