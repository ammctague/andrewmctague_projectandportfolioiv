//
//  Profile+CoreDataProperties.swift
//  Flip
//
//  Created by Andrew McTague on 3/5/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var name: String?
    @NSManaged public var email: String?
    @NSManaged public var password: String?

}
