//
//  Child+CoreDataProperties.swift
//  Flip
//
//  Created by Andrew McTague on 3/19/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//
//

import Foundation
import CoreData


extension Child {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Child> {
        return NSFetchRequest<Child>(entityName: "Child")
    }

    @NSManaged public var name: String?
    @NSManaged public var age: String?

}
