//
//  Child+CoreDataClass.swift
//  Flip
//
//  Created by Andrew McTague on 3/19/18.
//  Copyright © 2018 beardbanditcoding. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Child)
public class Child: NSManagedObject {

}
