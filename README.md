# AndrewMcTague_ProjectAndPortfolioIV

# Installation instructions
Open project via Xcode and use the simulator on iPhone 6,7,8 or if you have the correct device plug into computer and select from the drop down menu your device.

# Hardware considerations and requirements
iPhone 6,7,8 or Simulator.

# Any login requirements for testing
No current login requirements 

# List of known bugs
Some graphical errors involving text sizing image placement, Returning from profile screen to home screen is missing. The Mode Button needs better placement and formatting.

# Any other special requirements and considerations for testing
 N/A
